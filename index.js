require('dotenv').config();
const Discord = require('discord.js');
const bot = new Discord.Client();
const TOKEN = process.env.TOKEN;

bot.login(TOKEN);

bot.on('ready', () => {
  console.info(`Logged in as ${bot.user.tag}!`);
});

bot.on('message', msg => {
  if (msg.author.id != 722624884856979489) {
    if (msg.content === "~setup") {
      msg.guild.createEmoji('https://i.imgur.com/kcweyXH.png', 'andy1');
      msg.guild.createEmoji('https://i.imgur.com/wwC2RZf.jpg', 'andy2');
      msg.guild.createEmoji('https://i.imgur.com/ANwCgP3.png', 'andy3');
    }
    if (msg.author.id == 205120436428603393) {
      msg.react(getRandomEmoji(msg));
    }
    if (msg.content.toLowerCase().includes("andy") && msg.author.id != 205120436428603393) {
      var count = (msg.content.toLowerCase().match(/andy/g) || []).length
      var message = "";
      for (i = 0; i<count; i++) {
        message = message.concat(" ", getRandomEmoji(msg));
      }
      msg.channel.send(message);
    }
    if (msg.content.toLowerCase().includes("ping")) {
      msg.reply("pong!")
    }
  }
});


function getRandomEmoji(msg) {
  var emojiList = ["andy1", "andy2", "andy3"];
  var index = Math.floor(Math.random()*3);
  var query = emojiList[index];
  var emoji = msg.guild.emojis.find(emoji => emoji.name === query);
  return emoji;
}